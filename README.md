# vps-300p

Reverse engineering a controller unit for the VPS-300P video mixer

## Pins (quick notes in finnish)

1. Dataa mikseriltä, harvoin yksittäisiä pulsseja
2. Ei mitään? vedetty ylös, linkki?
3. Jotain sarjadataa 38400, 8N1, peilikuvana pinnissä 8 0x70 0x80 0x80 0x80 tuntuu olevan mitä siellä menee
4. Ei mitään? vedetty alas
5. T-bar, potikat serial 38400bps 8N1
6. Ei mitään? vedetty alas
7. Mystistä sarjadataa, ei ole 38400....
8. Jotain sarjadataa 38400, 8N1, peilikuvana pinnissä 3
9. 9
10. GND
11. 11
12. 12
13. 13
14. 14
15. 15
